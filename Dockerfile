FROM debian:bookworm-slim

# From version-gramps.txt
ARG GRAMPS_VERSION
ARG GRAMPS_REVISION

MAINTAINER Paul Walker <wpaulsg@gmail.com>

# Gramps default GRAMPSHOME set to user HOME and will populate HOME/gramps
ENV GRAMPSHOME /

RUN apt-get update && apt-get install --yes --no-install-recommends \
    ca-certificates\
    curl \
    gir1.2-gexiv2-0.10 \
    gir1.2-gtk-3.0 \
    gir1.2-osmgpsmap-1.0 \
    librsvg2-2 \
    python3-bsddb3 \
    python3-gi \
    python3-gi-cairo \
    python3-icu \
    xdg-utils \
    && rm -rf /var/lib/apt/lists/*

# https://github.com/gramps-project/gramps/releases
RUN curl -s -L "https://github.com/gramps-project/gramps/releases/download/v${GRAMPS_VERSION}/gramps_${GRAMPS_VERSION}-${GRAMPS_REVISION}_all.deb" \
    > /tmp/gramps.deb \
    && dpkg -i /tmp/gramps.deb \
    && rm -f /tmp/gramps.deb

ENTRYPOINT [ "/usr/bin/gramps" ]

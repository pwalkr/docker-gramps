# Docker Image for Gramps Genealogical Research Software

## Build

    docker build -t gramps - < Dockerfile

## Usage

To run a GUI application, you will need to authorize access to the X server
E.g.:

    $ xauth local:

... and network/display flags for `docker run`:

    --env DISPLAY
    --network host

You will need volumes to store the database and media files.
By default, `GRAMPSHOME` is set to `/`.
To use an existing databse, you will need to set `GRAMPSHOME` such that `$GRAMPSHOME/gramps` is the path to gramps' database.
E.g., if your database was in `$HOME/gramps` with media in `$HOME/Media`:

    --env GRAMPSHOME=$HOME
    --volume $HOME/Media:$HOME/Media
    --volume $HOME/gramps:$HOME/gramps

Full sample `docker run` for a new install with everything preserved in /opt/gramps

    $ docker run \
        --env DISPLAY \
        --network host \
        --volume /opt/gramps/gramps:/gramps \
        --volume /opt/gramps/media:/media \
        gramps

Any pictures/media in `$HOME/gramps/media` will be available to gramps for inclusion.
